function authMiddleWare(req, res, next) {
  if(!req.headers.authorization) return res.status(401).json({status: 'fail', message: 'Unauthorized!', data:null});

  let token = req.headers.authorization.split('Bearer ')[1];

  if (!token) return res.status(401).json({ status: 'fail', message: 'Token not detected', data: null })

  const User = require('../models/User');
  let user = new User();

  user.findUserUsingToken(token).then((users) => {
    if (!users.length) return res.status(401).json({ status: 'fail', message: 'Token invalid!', data: null });

    req.user = users[0];
    next();
  })

}

module.exports = authMiddleWare;