const Model = require('./Model');

class User extends Model{

  constructor() {
    super('users');
  }

  getUser(email) {
    return this.knex.table(this.table).where('email', email);
  }

  findUserUsingToken(token) {
    return this.knex.table(this.table).where('token', token);
  }
}

module.exports = User;