const knex = require('knex')(require('../database/knexfile'));

class Model {

  constructor(table) {
    this.knex = knex;
    this.table = table;
  }

  insert(data) {
    return this.knex.table(this.table).insert(data);
  }

  del(data) {
    return this.knex.table(this.table).where('id', '=', data.id).del();
  }

  update(data) {
    return this.knex.table(this.table).where('id', '=', data.id)
      .update(data);
  }

  find(id) {
    return this.knex.table(this.table).where('id', '=', id).first();
  }

  all() {
    return this.knex.table(this.table).select('*');
  }

  where() {
    return this.knex.where.apply(this.knex, arguments).table(this.table);
  }
}

module.exports = Model;