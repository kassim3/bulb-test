const Model = require('./Model');

class Todo extends Model {

  constructor() {
    super('todo');
  }
}

module.exports = Todo;