const express = require('express');
const router = express.Router();
const controller = require('../controllers/TodoController');

router.get('/all', controller.all.bind(controller));

router.route('/')
.get(controller.index.bind(controller))
.post(controller.add.bind(controller))

router.route('/:id')
.all(controller.validationCheck.bind(controller))
.get(controller.index.bind(controller))
.patch(controller.update.bind(controller))
.delete(controller.delete.bind(controller))

module.exports = router;