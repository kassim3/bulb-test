const express = require('express');
const router = express.Router();
const controller = require('../controllers/AuthController');

router.post('/register', controller.register.bind(controller));
router.post('/login', controller.login.bind(controller));

module.exports = router;