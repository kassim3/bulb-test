const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const knex = require('knex')(require('./database/knexfile'));
const cors = require('cors');

const authRouter = require('./routes/authRouter');
const todoRouter = require('./routes/todoRouter');
const authMiddleWare = require('./middleware/authMiddleWare');

let whitelist = process.env.CORS_WHITELIST.split()
let corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

knex.migrate.latest().then(() => {
  app.use(cors(corsOptions))
  app.use(bodyParser.json());
  app.use('/auth', authRouter);
  app.use(authMiddleWare);
  app.use('/todo', todoRouter);

  app.listen(3000);
}).catch((err) => {
  console.log(err)
})
