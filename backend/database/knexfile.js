// Update with your config settings.
module.exports = {
  client: 'mysql',
  connection: {
    host: 'mysql',
    user: 'homestead',
    port: 3306,
    password: 'secret',
    database: 'homestead'
  },
  // debug:true,
  migrations: {
    directory: [__dirname + '/migrations']
  },
};
