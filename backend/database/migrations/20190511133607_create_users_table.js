
exports.up = function(knex, Promise) {
  return knex.schema.createTable('users', function (table) {
    table.increments('id');
    table.string('email').unique();
    table.string('password');
    table.string('name');
    table.string('role');
    table.string('token');
  });
};

exports.down = function(knex, Promise) {
  
};
