
exports.up = function(knex, Promise) {
  return knex.schema.createTable('todo', function (table) {
    table.increments('id');
    table.string('title');
    table.string('description');
    table.integer('user_id').unsigned()
    table.foreign('user_id').references('users.id')
  });
};

exports.down = function(knex, Promise) {
  
};
