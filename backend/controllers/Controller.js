const validator = require('validator');

class Controller {

  constructor() {
    this.validator = validator;
  }
}

module.exports = Controller;