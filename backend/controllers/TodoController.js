const Controller = require('./Controller');
const Todo = require('../models/Todo');

class TodoController extends Controller {
  
  index(req, res) {
    
    if (req.item) return res.json({ status: 'success', data: req.item })

    let todo = new Todo();

    todo.where('user_id', req.user.id).then((list) => {
      return res.json({status: 'success', data: list})
    })
  }

  validationCheck(req, res, next) {
    let itemId = req.params.id;

    let todo = new Todo();
    todo.find(itemId).then((item) => {
      if (!item) return res.status(404).json({ status: 'fail', message: 'item list not found', data: null });

      if (req.user.role != 'admin' && item.user_id != req.user.id) return res.status(401).json({ status: 'fail', message: 'not authorized to view this item', data: null });

      req.item = item;

      next();
    })
    .catch((err) => {
      console.log(err)

      return res.status(500).json({ status: 'fail', message: 'server error could not update item', data: null })
    });
  }
  
  all(req, res) {
    if (req.user.role != 'admin') return res.status(401).json({ status: 'fail', data: null, message: 'Unathorized! User is not admin!' });

    let todo = new Todo();
    todo.all().then((list) => {
      return res.json({ status: 'success', data: list })
    })
  }

  add(req, res) {
    let title = req.body.title;
    let description = req.body.description;

    if(!description) return res.status(400).json({ status: 'fail', message: 'Please provide a description', data: null });

    let item = {
      title: title,
      description,
      user_id: req.user.id
    }

    let todo = new Todo();
    todo.insert(item).then((ids) => {
      item.id = ids[0];

      return res.json({ status: 'success', data: item })
    })
  }

  update(req, res) {
      let title = req.body.title;
      let description = req.body.description;
      let item = req.item;
      
      if(title) item.title = title;
      if(description) item.description = description;
      
      let todo = new Todo();
      todo.update(item).then(() => {

        return res.json({ status: 'success', data: item })
      });
  }

  delete(req, res) {
    let item = req.item;

    let todo = new Todo();
    todo.del(item).then(() => {
      return res.json({ status: 'success', data: null })
    });
  }
}

module.exports = new TodoController();