const Controller = require('./Controller');
const User = require('../models/User');
const bcrypt = require('bcryptjs');

class AuthController extends Controller{
 
  register(req, res) {
    
    let email = req.body.email;
    let password = req.body.password;
    let name = req.body.name;
    let role = req.body.role;

    if(!password || !email || !name || !role) return res.status(400).json({status: 'fail', message: 'Please provide the email, password, name, role', data: null});

    if (!this.validator.isEmail(email)) return res.status(400).json({status: 'fail', message: 'Please provide a valid email', 'data': null});

    let salt = bcrypt.genSaltSync(10);
    password = bcrypt.hashSync(password, salt);

    let user = new User();
    user.insert({ email, password,name,role}, ['email, name, role, id']).then((ids) => {
      res.json({status: 'success', message: null, data: {id: ids[0], email, name, role} })
    })
    .catch((err) => {
      console.log(err);
      res.status(400).json({ status: 'fail', message: 'User could not be added'});
    })
  }

  login(req, res) {
    let email = req.body.email;
    let password = req.body.password;

    if (!password || !email) return res.status(400).json({ status: 'fail', message: 'Please provide both the email and password', data: null });

    if (!this.validator.isEmail(email)) return res.status(400).json({ status: 'fail', message: 'Please provide a valid email', 'data': null });

    let user = new User();
    user.getUser(email).then((users) => {
      if (!users.length) return res.status(401).json({ status: 'fail', message: 'Invalid credentials', data: null });

      let userData = users[0];

      if (!bcrypt.compareSync(password, userData.password)) return res.status(401).json({ status: 'fail', message: 'Invalid credentials', data: null });

      userData.token = bcrypt.hashSync(userData.id.toString() + new Date().getTime().toString());

      user = new User();
      user.update(userData).then(() => {
        return res.json({ status: 'success', token: userData.token});
      })
    }).catch((err) => {
      console.log(err)
      return res.status(500).json({ status: 'error', message: 'Server error', data: null });
    })
  }
}

module.exports = new AuthController();