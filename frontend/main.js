
var mainApp = angular.module("app", ['ngRoute']);

mainApp.config(function ($routeProvider) {
  $routeProvider
    .when('/home', {
      templateUrl: 'home.html',
      controller: 'HomeController'
    })
    .when('/login', {
      templateUrl: 'login.html',
      controller: 'LoginController'
    })
    .otherwise({
      redirectTo: '/home'
    });
});

mainApp.constant('config', {
  url: 'http://localhost:3000',
});

mainApp.controller('HomeController', function ($scope, $location, $http, config) {
  let token = localStorage.getItem('token');

  if(!token) $location.path('/login');

  $scope.items = [];

  $http.get(config.url + '/todo', { headers: { Authorization: `Bearer ${token}`}})
  .then((res) => {
    $scope.items = res.data.data;
    $scope.items.forEach(item => {
      item.display = true
    });
  }, (err) => {
    console.log(err)
    if (err.status == 401) $location.path('/login');
  });

  $scope.search = function() {

    $scope.items.forEach(item => {
      if (item.description.indexOf($scope.searchValue) > -1 || !$scope.searchValue) item.display = true;
      else item.display = false;
    });
  }
});

mainApp.controller('LoginController', function ($scope, $http, $location, config) {

  $scope.login = function() {
    $http.post(config.url + '/auth/login', {email:$scope.email, password: $scope.password})
    .then((res) => {
      localStorage.setItem('token', res.data.token)
      $location.path('/home');
    }, 
    (err)=> {
      console.log(err);
    })
  }
});
